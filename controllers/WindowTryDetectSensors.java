package ca.wstyler.daq.controllers;

import ca.harvestrobotics.maraca.model.SensorEntity;
import ca.wstyler.daq.controllers.addons.TryDetectTask;
import ca.wstyler.daq.utilities.Constants;
import ca.wstyler.daq.utilities.Popup;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class WindowTryDetectSensors {
    @FXML
    public ImageView ivSpinner;
    public AnchorPane tryDetectAnchorPane;
    public Label tryText;

    private static List<SensorEntity> sensorForDiscover = new ArrayList<>();
    private static WindowTryDetectSensors instance;

    public static WindowTryDetectSensors getInstance() {
        if (instance == null) {
            instance = new WindowTryDetectSensors();
        }
        return instance;
    }

    public WindowTryDetectSensors() {
        super();
        instance = this;
    }

    @FXML
    void initialize() throws URISyntaxException {
        ivSpinner.setImage(new Image(Constants.WINDOW_SPINNER_LOADING_PATH));
        // Show count of sensors during discovering process
        tryText.setText("Trying to detect [" + this.sensorForDiscover.size() + "] sensors...");
        detectSensors();
    }

    // Set sensors for discovery from IPWindow
    public static void setSensorForDiscover(List<SensorEntity> sensors) {
        sensorForDiscover = new ArrayList<>();
        sensorForDiscover = sensors;
    }

    // Start task for sensors detection
    public static List<SensorEntity> detectSensors() throws URISyntaxException {
        Task<Void> task = null;
        task = new TryDetectTask(sensorForDiscover);
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
        return null;
    }

    // Get sensors from task and send they to IPWindow
    public static void doneDiscovery(List<SensorEntity> returnedSensors) throws Exception {
        WindowIPOpen.setSendedTrySensors(sensorForDiscover);
        WindowIPOpen.setReturnedTrySensors(returnedSensors);
        WindowIPOpen.checkSList(true);

        Stage fxmlStage = new Stage();
        String fxmlPath = Constants.WINDOW_IP_OPEN_FXML_PATH;
        String title = "Use Sensors";
        int width = Constants.WINDOW_IP_OPEN_WIDTH;
        int height = Constants.WINDOW_IP_OPEN_HEIGHT;
        boolean isResizable = false;
        boolean isModal = true;
        Popup.showFxml(fxmlStage, fxmlPath, title, width, height, isResizable, isModal);

        getInstance().closeThis();
    }

    private void closeThis() {
        Stage stage = (Stage) tryText.getScene().getWindow();
        stage.close();
    }

}
