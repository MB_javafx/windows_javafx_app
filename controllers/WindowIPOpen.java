package ca.wstyler.daq.controllers;

import ca.harvestrobotics.maraca.apis.WifiApi;
import ca.harvestrobotics.maraca.model.SensorEntity;
import ca.wstyler.daq.utilities.Constants;
import ca.wstyler.daq.utilities.Popup;
import ca.wstyler.daq.utilities.SensorEntityParser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class WindowIPOpen {
    private WifiApi wifiSensors;

    @FXML
    public Label errorLabel;

    @FXML
    public ImageView ivEight;
    public ImageView ivSeven;
    public ImageView ivSix;
    public ImageView ivFive;
    public ImageView ivFour;
    public ImageView ivThree;
    public ImageView ivTwo;
    public ImageView ivOne;
    @FXML
    public TextField tfLocationEight;
    public TextField tfLocationSeven;
    public TextField tfLocationSix;
    public TextField tfLocationFive;
    public TextField tfLocationFour;
    public TextField tfLocationThree;
    public TextField tfLocationTwo;
    public TextField tfLocationOne;
    @FXML
    public TextField tfIPEight;
    public TextField tfIPSeven;
    public TextField tfIPSix;
    public TextField tfIPFive;
    public TextField tfIPFour;
    public TextField tfIPThree;
    public TextField tfIPTwo;
    public TextField tfIPOne;
    @FXML
    public TextField tfPortEight;
    public TextField tfPortSeven;
    public TextField tfPortSix;
    public TextField tfPortFive;
    public TextField tfPortFour;
    public TextField tfPortThree;
    public TextField tfPortTwo;
    public TextField tfPortOne;
    @FXML
    public Button cancelBtn;
    public Button openIPFileBtn;
    public Button connectBtn;
    public Button doneBtn;
    @FXML
    public AnchorPane windowIPOpenAnchorPane;

    // Styles CSS
    private String defaultStyle;
    private String successStyle;
    private String errorStyle;

    private int countOfLocations = 0;
    private int countOfIPs = 0;
    private int countOfPorts = 0;

    private static boolean checkSensorsList = false;
    boolean nextStep = true;

    // Pattern for validation IP
    private static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");


    // Icons
    ArrayList<ImageView> ivStatusList = new ArrayList<>();
    // Location
    ArrayList<TextField> tfLocationsList = new ArrayList<>();
    // IP
    ArrayList<TextField> tfIPList = new ArrayList<>();
    // Port
    ArrayList<TextField> tfPortList = new ArrayList<>();
    // if success
    private Image imageSuccess = new Image(Constants.WINDOW_IP_OK_ICON_PATH);
    // if error
    private Image imageError = new Image(Constants.WINDOW_IP_NO_ICON_PATH);
    // if default
    private Image imageDefault = new Image(Constants.WINDOW_IP_DEFAULT_ICON_PATH);

    private List<String> tempLocations;
    private List<String> tempIPs;
    private List<String> tempPorts;
    // For sensors
    private static List<SensorEntity> sensors = new ArrayList<>();
    private static List<SensorEntity> reternedTrySensors = new ArrayList<>();
    private static List<SensorEntity> sendetTrySensors = new ArrayList<>();

    @FXML
    void initialize() {

        errorLabel.setVisible(false);

        this.defaultStyle =
                "-fx-background-color: white;" +
                        "-fx-border-color: #aeb7c3;" +
                        "-fx-border-radius: 3";

        this.successStyle =
                "-fx-background-color: #9ee46a;" +
                        "-fx-border-color: green;" +
                        "-fx-border-radius: 3";

        this.errorStyle =
                "-fx-background-color: #f55858;" +
                        "-fx-border-color: #a00000;" +
                        "-fx-border-radius: 3";

        connectBtn.setText("Discover");
        doneBtn.setDisable(true);

        // Set image list
        ivStatusList.add(ivOne);
        ivStatusList.add(ivTwo);
        ivStatusList.add(ivThree);
        ivStatusList.add(ivFour);
        ivStatusList.add(ivFive);
        ivStatusList.add(ivSix);
        ivStatusList.add(ivSeven);
        ivStatusList.add(ivEight);

        // Set locations list
        tfLocationsList.add(tfLocationOne);
        tfLocationsList.add(tfLocationTwo);
        tfLocationsList.add(tfLocationThree);
        tfLocationsList.add(tfLocationFour);
        tfLocationsList.add(tfLocationFive);
        tfLocationsList.add(tfLocationSix);
        tfLocationsList.add(tfLocationSeven);
        tfLocationsList.add(tfLocationEight);

        // Set IP list
        tfIPList.add(tfIPOne);
        tfIPList.add(tfIPTwo);
        tfIPList.add(tfIPThree);
        tfIPList.add(tfIPFour);
        tfIPList.add(tfIPFive);
        tfIPList.add(tfIPSix);
        tfIPList.add(tfIPSeven);
        tfIPList.add(tfIPEight);

        // Set port list
        tfPortList.add(tfPortOne);
        tfPortList.add(tfPortTwo);
        tfPortList.add(tfPortThree);
        tfPortList.add(tfPortFour);
        tfPortList.add(tfPortFive);
        tfPortList.add(tfPortSix);
        tfPortList.add(tfPortSeven);
        tfPortList.add(tfPortEight);

        if (!checkSensorsList) {
            this.startWithDefaultView();
        } else {
            this.startWithCheckList();
        }
    }

    private void startWithCheckList() {
        this.startWithDefaultView();
        connectBtn.setText("Retry");
        doneBtn.setDisable(false);

        for (int i = 0; i < sensors.size(); i++) {
            tfLocationsList.get(i).setText(sensors.get(i).getName());
            tfIPList.get(i).setText(sensors.get(i).getIp());
            tfPortList.get(i).setText(String.valueOf(sensors.get(i).getPort()));

            if (reternedTrySensors.contains(sensors.get(i))) {
                ivStatusList.get(i).setImage(imageSuccess);
                tfLocationsList.get(i).setStyle(this.successStyle);
                tfIPList.get(i).setStyle(this.successStyle);
                tfPortList.get(i).setStyle(this.successStyle);
            } else {
                ivStatusList.get(i).setImage(imageError);
                tfLocationsList.get(i).setStyle(this.errorStyle);
                tfIPList.get(i).setStyle(this.errorStyle);
                tfPortList.get(i).setStyle(this.errorStyle);
            }
        }

    }

    public static void setReturnedTrySensors(List<SensorEntity> rTS) {
        reternedTrySensors = rTS;
    }

    public static void setSendedTrySensors(List<SensorEntity> sTS) {
        sensors = sTS;
    }

    private void startWithDefaultView() {
        errorLabel.setText("");
        errorLabel.setVisible(false);

        connectBtn.setText("Discover");
        doneBtn.setDisable(true);

        // No result styles
        // for location
        tfLocationsList.forEach(item -> {
            item.setStyle(this.defaultStyle);
        });
        // for ip
        tfIPList.forEach(item -> {
            item.setStyle(this.defaultStyle);
        });
        // for port
        tfPortList.forEach(item -> {
            item.setStyle(this.defaultStyle);
        });

        // No text
        // for location
        tfLocationsList.forEach(item -> {
            item.setText("");
        });
        // for ip
        tfIPList.forEach(item -> {
            item.setText("");
        });
        // for port
        tfPortList.forEach(item -> {
            item.setText("");
        });

        // No status images
        ivStatusList.forEach(item -> {
            item.setImage(this.imageDefault);
        });

    }

    // CANCEL
    public void cancelBtnEvent(ActionEvent actionEvent) {
        Popup.closeFxmlByActionEvent(actionEvent);
    }

    // OPEN FILE
    public void openIPFileBtnEvent(ActionEvent actionEvent) throws FileNotFoundException {
        File selectedFile = SensorEntityParser.selectTXTFile(windowIPOpenAnchorPane);
        if (selectedFile != null) {
            this.startWithDefaultView();
            this.readFileLines(selectedFile.getPath());
        }
    }

    private void readFileLines(String filePath) throws FileNotFoundException {
        this.clearTempLists();
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        try {
            String line = br.readLine();
            while (line != null && !line.isEmpty()) {
                this.parseLineToUITable(line);
                line = br.readLine();
            }
            if (line == null) {
                // Show table
                for (int i = 0; i < this.tempLocations.size(); i++) {
                    this.tfLocationsList.get(i).setText(this.tempLocations.get(i));
                }

                for (int i = 0; i < this.tempIPs.size(); i++) {
                    this.tfIPList.get(i).setText(this.tempIPs.get(i));
                }

                for (int i = 0; i < this.tempPorts.size(); i++) {
                    this.tfPortList.get(i).setText(this.tempPorts.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearTempLists() {
        this.tempLocations = new ArrayList<>();
        this.tempIPs = new ArrayList<>();
        this.tempPorts = new ArrayList<>();
        sensors = new ArrayList<>();
        reternedTrySensors = new ArrayList<>();
        sendetTrySensors = new ArrayList<>();
    }

    public void parseLineToUITable(String line) {
        List<String> data = Arrays.asList(line.split(","));

        this.tempLocations.add(data.get(0));
        this.tempIPs.add(data.get(1));
        this.tempPorts.add(data.get(2));
    }


    // DONE
    public void doneBtnEvent(ActionEvent actionEvent) throws Exception {
        errorLabel.setVisible(false);
        countOfLocations = 0;
        countOfIPs = 0;
        countOfPorts = 0;

        this.tfLocationsList.forEach(item -> {
            if (!Objects.equals(item.getText().trim(), "")) {
                countOfLocations++;
            }
        });
        this.tfIPList.forEach(item -> {
            if (!Objects.equals(item.getText().trim(), "")) {
                countOfIPs++;
            }
        });
        this.tfPortList.forEach(item -> {
            if (!Objects.equals(item.getText().trim(), "")) {
                countOfPorts++;
            }
        });

        if (countOfLocations == countOfIPs && countOfLocations == countOfPorts) {
            if (countOfLocations > 0) {

                if (checkSensorsList) {
                    WindowDnDSensors.setSensors(reternedTrySensors);
                } else {
                    WindowDnDSensors.setSensors(sensors);
                }

                Stage fxmlStage = new Stage();
                String fxmlPath = Constants.WINDOW_DND_FXML_PATH;
                String title = "Use Sensors";
                int width = Constants.WINDOW_DND_WIDTH;
                int height = Constants.WINDOW_DND_HEIGHT;
                boolean isResizable = false;
                boolean isModal = true;
                Popup.showFxml(fxmlStage, fxmlPath, title, width, height, isResizable, isModal);
                // close itself;
                Popup.closeFxmlByActionEvent(actionEvent);
            } else {
                System.out.println("Empty input");
            }
        } else {
            System.out.println("Invalid input");
        }
    }

    // DISCOVER/RETRY
    public void connectBtnEvent(ActionEvent actionEvent) throws Exception {
        errorLabel.setText("");
        errorLabel.setVisible(false);
        this.clearTempLists();
        countOfLocations = 0;
        countOfIPs = 0;
        countOfPorts = 0;
        nextStep = true;

        this.tfLocationsList.forEach(item -> {
            item.setStyle(this.defaultStyle);
            if (!Objects.equals(item.getText().trim(), "")) {
                countOfLocations++;
            }
        });
        this.tfIPList.forEach(item -> {
            item.setStyle(this.defaultStyle);
            if (!Objects.equals(item.getText().trim(), "")) {
                countOfIPs++;
            }
        });
        this.tfPortList.forEach(item -> {
            item.setStyle(this.defaultStyle);
            if (!Objects.equals(item.getText().trim(), "")) {
                countOfPorts++;
            }
        });
        this.ivStatusList.forEach(item -> item.setImage(imageDefault));

        if (countOfLocations == countOfIPs && countOfLocations == countOfPorts) {

            errorLabel.setVisible(false);

            if (countOfLocations > 0) {
                // LOCATION
                for (int i = 0; i < tfLocationsList.size(); i++) {
                    this.tfLocationsList.get(i).setStyle(this.defaultStyle);
                    if (!Objects.equals(this.tfLocationsList.get(i).getText().trim(), "")) {
                        // LOCATIONS VALIDATION
                        if (!labelsValidation(this.tfLocationsList.get(i))) {
                            nextStep = false;
                            this.tfLocationsList.get(i).setStyle(this.errorStyle);
                            errorLabel.setText("The value you entered is not valid.");
                            errorLabel.setVisible(true);
                        }

                        // CHECK REPEAT LABELS L/R1-4
                        checkRepeatLabels(this.tfLocationsList);

                        // IP VALIDATION
                        if (!ipValidation(this.tfIPList.get(i))) {
                            nextStep = false;
                            this.tfIPList.get(i).setStyle(this.errorStyle);
                            errorLabel.setText("The value you entered is not valid.");
                            errorLabel.setVisible(true);
                        }

                        // CHECK REPEAT IP
                        checkRepeatLabels(this.tfIPList);

                        // PORT VALIDATION
                        if (!portsValidation(this.tfPortList.get(i))) {
                            nextStep = false;
                            this.tfPortList.get(i).setStyle(this.errorStyle);
                            errorLabel.setText("The value you entered is not valid.");
                            errorLabel.setVisible(true);
                        }

                        SensorEntity sensor = new SensorEntity(
                                this.tfIPList.get(i).getText(),
                                this.tfLocationsList.get(i).getText(),
                                Integer.parseInt(this.tfPortList.get(i).getText()));
                        sensors.add(sensor);

                    }
                }


                if (nextStep) {
                    WindowTryDetectSensors.setSensorForDiscover(sensors);

                    Stage fxmlStage = new Stage();
                    String fxmlPath = Constants.WINDOW_TRY_DETECT_FXML_PATH;
                    String title = "Use Sensors";
                    int width = Constants.WINDOW_TRY_DETECT_WIDTH;
                    int height = Constants.WINDOW_TRY_DETECT_HEIGHT;
                    boolean isResizable = false;
                    boolean isModal = true;
                    Popup.showFxml(fxmlStage, fxmlPath, title, width, height, isResizable, isModal);
                    // close itself;
                    Popup.closeFxmlByActionEvent(actionEvent);
                }

            } else {
                errorLabel.setText("No values entered.");
                errorLabel.setVisible(true);
            }
        } else {
            errorLabel.setVisible(true);
            errorLabel.setText("Incomplete input.");
            errorLabel.setVisible(true);
        }
    }

    static void checkSList(boolean i) {
        checkSensorsList = i;
    }

    // VALIDATION LABELS L/R1-4
    private boolean labelsValidation(TextField tfLocation) {
        int iter = 0;
        for (int n = 1; n < 5; n++) {
            if (Objects.equals(tfLocation.getText(), "L" + n) ||
                    Objects.equals(tfLocation.getText(), "R" + n)) {
                iter++;
            }
        }
        return iter != 0;
    }

    // CHECK REPEAT LABELS L/R1-4
    // CHECK REPEAT IP ADDRESS
    private void checkRepeatLabels(ArrayList<TextField> tfList) {
        ArrayList<TextField> tempList = new ArrayList<>();

        tfList.forEach(item -> {
            if (!Objects.equals(item.getText().trim(), "")) {
                tempList.add(item);
            }
        });

        ArrayList<TextField> dublicates = new ArrayList<>();

        for (int i = 0; i < tempList.size(); i++) {
            for (int j = i + 1; j < tempList.size(); j++) {
                if (Objects.equals(tempList.get(i).getText(), tempList.get(j).getText())) {
                    dublicates.add(tempList.get(i));
                    dublicates.add(tempList.get(j));
                }
            }
        }

        if (dublicates.size() > 0) {
            dublicates.forEach(text -> text.setStyle(this.errorStyle));
            nextStep = false;
            errorLabel.setText("Duplicate entry.");
            errorLabel.setVisible(true);
        }

    }

    // VALIDATION IP ADDRESS
    private boolean ipValidation(TextField tfLocation) {
        return PATTERN.matcher(tfLocation.getText()).matches();
    }

    // VALIDATION PORTS
    private boolean portsValidation(TextField tfLocation) {
        String mString = tfLocation.getText();
        boolean val;
        try {
            int fooInt = Integer.parseInt(mString);
            val = fooInt > 0 && fooInt < 65536;
        } catch (NumberFormatException e) {
            val = false;
        }
        return val;

    }

}
