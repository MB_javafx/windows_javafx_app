package ca.wstyler.daq.controllers.addons;

import ca.harvestrobotics.maraca.apis.WifiApi;
import ca.harvestrobotics.maraca.model.SensorEntity;
import ca.wstyler.daq.controllers.MainView;
import ca.wstyler.daq.controllers.WindowTryDetectSensors;
import javafx.concurrent.Task;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class TryDetectTask extends Task<Void> {
    private static List<SensorEntity> sensorForDiscover = new ArrayList<>();
    private List<SensorEntity> sensorForReturn = new ArrayList<>();
    private WifiApi wifiApi = MainView.getInstance().getWifiApi();

    // Get sensors from WindowTryDetectSensors
    public TryDetectTask(List<SensorEntity> sensorFDisc) throws URISyntaxException {
        sensorForDiscover = sensorFDisc;
    }

    @Override
    protected Void call() {
        if (MainView.getInstance().getWifiApi() != null) {
            wifiApi.start();
            wifiApi.stop();
            wifiApi = null;
        }

        // WifiApi is a specific library for this project
        wifiApi = new WifiApi(sensorForDiscover);
        MainView.getInstance().setWifiApi(wifiApi);
        wifiApi.registerSensors();
        return null;
    }

    @Override
    protected void failed() {
        sensorForReturn = wifiApi.getRegisteredSensors();
        try {
            // Return the sensors that are connected
            WindowTryDetectSensors.doneDiscovery(sensorForReturn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void succeeded() {
        sensorForReturn = wifiApi.getRegisteredSensors();
        try {
            // Return the sensors that are connected
            WindowTryDetectSensors.doneDiscovery(sensorForReturn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
