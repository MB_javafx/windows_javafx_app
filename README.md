# JavaFX example

Example code from the project of application for tablets on Windows OC

The application has the ability to open a file with a list of IP addresses in a special window (IPOpenWindow). IPOpenWindow performs validation of the supported positions for each sensor, validation of the address and port. When you click on the "Discovery" button, the sensor registration process is started using a special library and API sensors. During sensor registration, a window with a spinner and text showing the number of sensors to which the connection is connected is opened. After registering the sensors, the IPOpenWindow window opens again in which sensors it turned out to be connected in green and red ones that they could not connect to.

---

You can see the images of these windows in the folder `<controllers/addons/images/>`